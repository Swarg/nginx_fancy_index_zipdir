# About

Goal: Adding the ability to download the entire directories with Fancy_Index
module of Nginx. This simple php script add buttons into each page of
the index of files into already configured File-Server.

[fancy_index](https://www.nginx.com/resources/wiki/modules/fancy_index/)


## How it works

Add buttons to footer of Fancy_Index
- Create Archive   --  Create a archive of current opened directory (by URL)
- Delete Archive   --  Delete already existed archive of current directory.
- Download Archive --  Download already created archive of current directory

Steps:
- You just go to the directory you want to download as one zip-archive
- Press the button "Create Archive"
- The page is being updated (Browser hung up while the archive is being created)
- an archive of the entire directory appears in the footer of the Index page.
- After downloading, you can delete the archive from the server via the button
  "Delete Archive"
- If you want the archive to be downloaded immediately and automatically after
  creation, use the checkbox near to the "Create Archive" button.


### Restrictions

- You cannot create an archive of the root directory of a file server.
- Script by default use `/tmp/` as a temporary directory to create archives
  and move it after creating into the appropriate directory. (It Configurable)
- When you create an archive from a heavy directory, the browser will hang
  waiting for a response from the server until the archive is created.
- For convenience, you can use `AutoDownload` checkbox near to the
  `Create Archive` button to automatically load the created archive immediately
  after it will be created.
- But at the same time, when you check `AutoDownload` checkbox the auto-update
  of the file index does not occur. That's why to see the new archive in the
  index of files, you need to refresh the page manually.


## Manual Installation for Fancy_Index + Nginxy-Theme

Use Case: Has Already configured Nginxy-Theme with Fancy_Index
update it manually without ./install.sh.

Steps:
1. Add html-form into alredy exist footer.html
2. Add ngixn-location for php-script into your site ngix config
3. Add php-script into fileserver

If you use the Nginxy-Theme with Fancy_Index when for installing this script
into your FileServer you can:

First. Edit the footer.html file.
just copy the entire contents of this file [html/footer.html](./html/footer.html)
into an your existing file `<path/to/your/html>/Nginxy-Theme/footer.html`
Paste like this:
Nginxy-Theme/footer.html:
```html
	<!-- End of nginx output -->
        </div>
      </div>
      <div id="footer" class="row">
      <center >
        <!-- This footer will change depending on your settings in top.html -->

        <style>...</style>
        <form ... >
        <!--
            copy the whole contents of the file html/footer.hml and paste it here
        -->
        </form>
        <script>...</script>

      </center>
      </div>
    </div>
  </body>
</html>
```

2. Add location for php-script into your fileserv nginx config-file:
```
    location = /fancyindex-logic.php {
        include snippets/fastcgi-php.conf;
        fastcgi_pass unix:/run/php/php-fpm.sock;
        root = /srv/php-scripts/fancyindex/           # SCRIPTS_DIR/FI_APP_NAME
    }
    location = /fancyindex-render.php {
        include snippets/fastcgi-php.conf;
        fastcgi_pass unix:/run/php/php-fpm.sock;
        root = /srv/php-scripts/fancyindex/           # SCRIPTS_DIR/FI_APP_NAME
    }
```
Note: here `unix:/run/php/php-fpm.sock` is the unix socket for php-fpm
to see that used in your system use:
```sh
sudo systemctl status php-fpm | grep 'Process.*install'
```
OR
```sh
sudo systemctl status php8.1-fpm | grep 'Process.*install'
```
Output:
```
Process: 79887 ExecStartPost=/usr/lib/php/php-fpm-socket-helper install
/run/php/php-fpm.sock /etc/php/8.1/fpm/pool.d/www.conf ...
^^^^^^^^^^^^^^^^^^^^^
```

3. Copy [php-scripts](./src/) into `SCRIPTS_DIR/FI_APP_NAME` on your FileServ
```sh
cp -r ./src /srv/php-scripts/fancyindex/
chown -R www-data:www-data /srv/php-scripts/fancyindex/src
chmod -R 440 /srv/php-scripts/fancyindex/src
```
Notice: For security reasonse, keep the script directory outside of files
available for static downloading.
That's why I'm moving the script directory outside of the file server here.


## install.sh

Goal: deployment automation and helper for installing integration

There are two ways to use:
- install standalon with Fancy_Index (without Nginxy-Theme)
- install integration with Nginxy-Theme + Fancy_Index
- It is possible to only see what will be done, but do nothing `--dry-run`


## Installation via install.sh script

```sh
ssh <to-you-nginx-fileserver>
git clone https://gitlab.com/Swarg/nginx_fancy_index_zipdir
cd nginx_fancy_index_zipdir
chmod u+x ./install.sh
```

### First Way: integration with already installed Nginxy-Theme+Fancy_Index

```sh
./install.sh nginxy   # install script by default values
```
This will automatically do:
<!-- TODO auto inset the form-block with buttons into Nginxy-Theme footer.html -->
- copy footer.html with buttons into Nginxy-Theme Directory (`NGINXY_PATH`)
  and replace the older one `Nginxy-Theme footer.html file`
- copy `html/js/archived_dir.js` and `html/css/buttons.css into` `NGINXY_PATH`
- copy php-scripts into right place

After these steps, it will remain to do yourself:
- Add locations for fancyindex-{logic,render}.php into nginx-config

Notes:
- To see 'right place for code' and to see all used EnvVariables used in
  installation you can use command: `./install.sh --show-vars`
- Destination dir for scipts defined by combination: `$SCRIPTS_DIR/$FI_APP_NAME/src/`


### Second Way: standalon html-files with Fancy_Index only

```sh
./install.sh standalon                     # install script with default values
```
This will automatically do:
- copy html-files
- copy php-scripts into right place by default values

After these steps, it will remain to do yourself:
- Add locations for static html-files
- Add locations for fancyindex-{logic,render}.php into nginx-config


### How to See all supported Customizable Env Variables

This command only show EnvVars and its default values:
```sh
./install.sh --show-vars
```
Output: there printed a defaults values of all supported EnvVars:
```
[FI_APP_NAME]  :  fancyindex
[HIDDEN]       :  /.nginx
[SCRIPTS_DIR]  :  /srv/php-scripts

Path to install script:
Scripts(php)   :  /srv/php-scripts/fancyindex/src
                  $SCRIPTS_DIR/$FI_APP_NAME/src

Path to install static-html files:
Static(html)   :  /srv/fileserv//.nginx/fancyindex
                  $FILESERV_ROOT/$HIDDEN/$FI_APP_NAME

Path to root-dir of your File Server
[FILESERV_ROOT]:  /srv/fileserv

Nginxy-Theme for FancyIndex (Optional) used only for ./install.sh nginxy
[NGINXY_PATH]  :  /.nginxy
  (full path)  :  /srv/fileserv//.nginxy
               :  $FILESERV_ROOT/$NGINXY_PATH
```

You can customize installations paths to your own paths using EnvVars like this:

```sh
FILESERV_ROOT="/srv/root-dir-of-my-fileserv" \
SCRIPTS_DIR="/srv/my-script-dir" \
FI_APP_NAME="fi" \
HIDDEN=".www" \
./install.sh --show-vars
```
Output :
```
[FI_APP_NAME]  :  fi
[HIDDEN]       :  .www
[SCRIPTS_DIR]  :  /srv/my-script-dir

Path to install script:
Scripts(php)   :  /srv/my-script-dir/fi/src
                  $SCRIPTS_DIR/$FI_APP_NAME/src

Path to install static-html files:
Static(html)   :  /srv/root-dir-of-my-fileserv/.www/fi
                  $FILESERV_ROOT/$HIDDEN/$FI_APP_NAME

Path to root-dir of your File Server
[FILESERV_ROOT]:  /srv/root-dir-of-my-fileserv

Nginxy-Theme for FancyIndex (Optional) used only for ./install.sh nginxy
[NGINXY_PATH]  :  /.nginxy
  (full path)  :  /srv/root-dir-of-my-fileserv//.nginxy
               :  $FILESERV_ROOT/$NGINXY_PATH
```

You can also see where and what will be installed, without the installation
itself by using flag `--dry-run`:
```sh
FILESERV_ROOT="/srv/root-dir-of-my-fileserv" \
SCRIPTS_DIR="/srv/my-script-dir" \
FI_APP_NAME="fi" \
HIDDEN=".www" \
./install.sh --show-var <standalon|nginxy> --dry-run
```



To autogenerate locations parts for nginx-config you can use:

For defaults::
```sh
./install.sh --show-location
```

With specified values for paths::
```sh
FILESERV_ROOT="/srv/root-dir-of-my-fileserv" \
SCRIPTS_DIR="/srv/my-script-dir" \
FI_APP_NAME="fi" \
HIDDEN=".www" \
./install.sh --show-locations
```
Output:
```nginx
    location / {
        fancyindex on;
        fancyindex_exact_size off;
        fancyindex_header .www/fi/header.html;
        fancyindex_footer .www/fi/footer.html;
        fancyindex_css_href .www/fi/style.css;
        fancyindex_time_format "%B %e, %Y";
        try_files $uri.html $uri $uri/ =404;
        root /srv/root-dir-of-my-fileserv;
    }

    set $fi_root /srv/my-script-dir/fi/src

    location = /fancyindex-logic.php {
        include snippets/fastcgi-php.conf;
        fastcgi_pass unix:/run/php/php-fpm.sock;
        root ;
    }
    location = /fancyindex-render.php {
        include snippets/fastcgi-php.conf;
        fastcgi_pass unix:/run/php/php-fpm.sock;
        root ;
    }

# Note: include snippests/fastcgi-php.conf - use file from nginx config:
/etc/nginx/snippets/fastcgi-php.conf
```


## Example of nginx config for work with Fancy_Index

```nginx
server {
    listen 80;
    listen [::]:80;    # IPv6
    server_name _;     # here your domain
    charset utf-8;

    location / {
        fancyindex on;
      	fancyindex_exact_size off;
      	fancyindex_header /.nginx/fancyindex/header.html;  # HIDDEN/$FI_APP_NAME
        fancyindex_footer /.nginx/fancyindex/footer.html;
      	fancyindex_css_href /.nginx/fancyindex/style.css;
      	fancyindex_time_format "%B %e, %Y";
      	try_files $uri.html $uri $uri/ =404;
        root /srv/fileserv;                                # FILESERV_ROOT
    }

    location = /fancyindex-logic.php {
        # include snippets/fastcgi-php.conf;
        fastcgi_pass unix:/run/php/php-fpm.sock;    # Your path to php-fpm sock
        root = /srv/php-scripts/fancyindex/           # SCRIPTS_DIR/FI_APP_NAME
    }

    location = /fancyindex-render.php {
        fastcgi_pass unix:/run/php/php-fpm.sock;    # Your path to php-fpm sock
        root = /srv/php-scripts/fancyindex/           # SCRIPTS_DIR/FI_APP_NAME
    }

    location /checkhealth {
      return 200 "healthy\n";  # Simples way to check your site-configuration
    }

    # location = /phpinfo.php {
    #    include snippets/fastcgi-php.conf;
    #    fastcgi_pass unix:/run/php/php-fpm.sock;
    #    root = /srv/php-scripts/fancyindex/
    # }
}
```


## Directory Compression Settings
These settings impact on:
- archive format
- archive location on the server

You can edit Settings-Section in this file [src/config.php](./src/config.php)

For Example you can choose command to compress directory (`COMPRESS_CMD`)
and temporary directory to creating archive file(`TMP_DIR`)

By default, the creation of directory archives will go to a separate directory
named `STORAGE_DIR` at the root of the fileserver.
This can be useful when the shared files accessed only for read-only permissions
and not accesseble for writing.
So nginx and php will not be able to place dynamicaly generated archives in
these Indexed directories.

If you want directory archives to be placed in the same Index directory,
for which the archive is being created set the value `STORAGE_DIR` to empty
string, like this:
```
define('STORAGE_DIR', '');
```
and edit(sync) value of `STORAGE_DIR` in the javascript file here:
[html/js/archived_dir.js](html/js/archived_dir.js)
```js
  const STORAGE_DIR = ''
```
Then all dynamically created archives after creation will be placed in the same
directory for which they were created.
And immediately after creation, they can be seen in the autoindex list.


`COMPRESSED_DB` - keep track of all created dir-archives in one place as
a simple list with full paths to archives.

[See src/config.php](./src/config.php)
```php
<?
##########################     SETTINGS      ##################################

define('TMP_DIR',       "/tmp/");            # temp file for archiving directories
define('FILESERV_ROOT', "/srv/fileserv/");   # root of file-server
define('STORAGE_DIR',   ".archived_dirs");   # dirname for archived dirs
define('CSS_STYLE',     "/.nginxy/css/style.css");

define('DIRNAME_PREFIX', '_');
define('COMPRESSED_DB',  "/tmp/compressed.db");

define('ARCHIVE_EXT',    "zip");
define('COMPRESS_CMD',   "zip -rq %outname %path-to-zip");
define('COMPRESS_CMD_IGNORE', [  # Ignored exits code to continue work
    # When you try to compress directory with some count of not readable files
    # Useful then you want to confinue compression and skip not readable files.
    18 # man zip:  Warning: "zip could not open a specified file to read"
]);
# ...
```

## CSS for buttons

By default `footer.html` used css with path `/.nginxy/css/buttons.css`.
So if you have setup with another path to Nginxy-Theme when
you need to fix this path manyally.
<!-- TODO add auto setup rigth value into install.sh s|n -->

Add link to buttons.css into your header.html:
```html
<head>
  ...
  <link rel="stylesheet" href="/.nginxy/css/buttons.css" type="text/css"/>
</head>
```
and remove from footer.html this line:
```html
<style type="text/css"> @import url("/.nginxy/css/buttons.css"); </style>
```


# Additional notes

Useful when you need to manually check file access on behalf of a user::
```sh
sudo su -l www-data -s /bin/bash
```

Remember that for the script to work correctly, the web server must have access
to the files::
```sh
sudo chown -R www-data:www-data /srv/fileserv/
```

## Check Health

replace localhost to your domain

```sh
curl http://localhost/fancyindex-logic.php?action=checkhealth
```

## Security issues

- TODO:

You need to configure the site so that the web server cannot give the scripts
themselves as static.
When you use nginx and php-fpm, each of them can work on behalf of a specific user.
It looks like the best solution would be to move php-fpm to a separate user
and configure access to the directory with php-scripts only for php-fpm,
completely restricting access for web-server process from reading from there.

<!--
rsync -r * zv:~/nginx-dd/
-->

##  How to change User or Group of your php
https://unix.stackexchange.com/questions/30190/how-do-i-set-the-user-of-php-fpm-to-be-php-user-instead-of-www-data

### Debian-Based

See config file `/etc/php/8.1/fpm/pool.d/www.conf`
There you will find options user and group.
It will appear as [www]. You can make it into [myuser] group=mygroup.

by default used:
```
[www]
; ...
user = www-data
group = www-data
```

### RedHat-Based(CentOS)
See config file like `/etc/php-fpm.d/www.conf`
change the definitions of user and group:
```
;default user and group
user = apache
group = apache
```


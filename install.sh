#!/bin/bash
#
# Goal: Install Scripts and Html-files into already installed nginx server
# configured as FileServer with Fancy_Index module.
# (Copy to right place with right permissions)

# FILESERV_ROOT="/srv/fileserv"
# If variable not set or null, set it to default.
FILESERV_ROOT="${FILESERV_ROOT:=/srv/fileserv}"
# Goal: hold php-scripts outside your static files
# To Prevent uploading as static files.
# TODO Permission to read only for PHP-FPM user not for nginx or apache user
SCRIPTS_DIR="${SCRIPTS_DIR:=/srv/php-scripts}"

FI_APP_NAME="${FI_APP_NAME:=fancyindex}"
# HIDDEN="../nginx-inernal-www"       # outside of you fileserv       not work
HIDDEN="${HIDDEN:=/.nginx}"           # warn it inside your file-server!
# TODO: find way to specify path for fancyindex_footer outside of location root

NGINXY_PATH="${NGINXY_PATH:=/.nginxy}"

WWW_DIR_RELATIVE="$HIDDEN/$FI_APP_NAME"
WWW_DIR="$FILESERV_ROOT/$WWW_DIR_RELATIVE"

NGINX_USER="${NGINX_USER:=www-data}"
PHP_FPM_USER="${PHP_FPM_USER:=www-data}"
# Permissions 440- Read only for user and group
PERM="${PERM:=440}"         # for PHP-Scripts TODO: read only for User(PHP-FPM)
PERM_WWW="${PERM_WWW:=440}"
PERM_CONF="${PERM_CONF:=640}"

CMD=$(basename $0)


if [ -z "$1" -o "$1" == "-h" -o "$1" == "--help" ]; then
  OPTS="[-v|--show-vars] [-l|--show-locations] "
  CMD_OPTS="[-c|--skip-config] [-d|--dry-run]"
  echo "Installator of Downloading Directories Script for Nginx + FancyIndex"
  echo "PreRequiries: Already installed Fancy_Index (and optionaly Nginxy-Theme)"
  echo ""
  echo "./$CMD <s|standalone>  -- install for using with Fancy_Index without Ngingy-Theme"
  echo "Usage Example:"
  echo " ./$CMD $OPTS standalone $CMD_OPTS"
  echo ""
  echo "./$CMD <n|nginxy>       -- install for using with Fancy_Index & Nginxy-Theme"
  echo "Usage Example:"
  echo " ./$CMD $OPTS nginxy $CMD_OPTS"
  echo "Way to Specify Custom Directory with Nginxy-Theme:"
  echo " NGINXY_PATH=/.nginxy ./$CMD nginxy [-l] [-v] [-c] [-d]"
  echo ""
  echo "OptKeys:"
  echo " -l|--show-vars       --  show configured variables for installation"
  echo " -l|--show-locations  --  show part of nginx config for server block"
  echo "OptKeys for installation commands only:"
  echo " -c|--skip-config     --  do not update src/config.php(Keep old config)"
  echo " -d|--dry-run         --  only show what will be done but do nothing"
  echo ""
  echo "To generate a piece of the NGINX config for a 'server' block "
  echo "whith your of defaults Vars you can use this OptKey separately:"
  echo "./$CMD <-l|--show-locations>  --  only show part of nginx config"
  exit 0
fi

##############################  UTILS  ########################################
function isDryRun() {
  if [ ! -z "$DRY_RUN" ]; then
    return 0;  # true
  else
    return 1;  # false
  fi
}

# $@ check all opts for --dry-run  and set global DRY_RUN variable
function checkAndDefineDryRun() {
  DRY_RUN=""
  while [ "$1" != "" ]; do
    if [ "$1" == "-d" -o "$1" == "--dry-run" -o "$1" == "-dry-run" ]; then
      DRY_RUN="1"
      echo "=================================================================="
      echo "==       [DRY_RUN MODE]     ONLY SHOW ACTIONS DO NOTHING!       =="
      echo "=================================================================="
      return 0; # true
    fi
    shift
  done
  return 1; # false
}

# Insert Lines from one file into another and print result into StdOut
# Lines will be inserted after the line containing the specified anhor
# $1 - file with lines (part) to insertion into dst file -- what we insert
# $2 - dst file  where we insert
# $3 - anhor after which we insert lines from $1
function ins2file_after_anhor() {
  local ins_file="$1"
  local dst_file="$2"
  local ANHOR="$3"
  if [ -z "$ANHOR" ]; then
    # echo "ERROR! 1"
    return 1;
  fi
  if [ ! -f  "$ins_file" -o ! -f "$dst_file" ]; then
    # echo "ERROR! 2"
    return 2; # error not found one of the files
  fi

  IFS=$'\n' read -d '' -r -a INS_ARRAY < "$ins_file"
  IFS=$'\n' read -d '' -r -a DST_ARRAY < "$dst_file"

  PREV_IFS=$IFS
  IFS=$'\n'
  for line in ${DST_ARRAY[@]}; do
    echo $line
    if [[ $line == *"$ANHOR" ]]; then
      for s in ${INS_ARRAY[@]}; do
        echo $s
      done
    fi
  done
  IFS=$PREV_IFS
  return 0; # success
}

# $1 user
# $2 dirname
function mkdir_ex() {
  [ -z "$1" ] && echo "Bad User" && exit 1
  [ -z "$2" ] && echo "Empty Dir" && exit 2
  local USER="$1"
  local DIR="$2"
  if isDryRun; then
    echo "sudo mkdir -p $DIR && sudo chown $USER:$USER $DIR"
  else
    sudo mkdir -p "$DIR" && sudo chown "$USER:$USER" "$DIR"
  fi
}

# $1 - user
# $2 - permissions
function cp_ex() {
  [ -z "$1" ] && echo "Bad User" && exit 1
  [ -z "$2" ] && echo "Bad Permission" && exit 2
  u="$1"
  p="$2"
  src="$3"
  dst="$4"$(basename "$3")
  if [ ! -f "$src" ]; then
    echo "ERROR: Not Found file $src" ; exit 1
  fi
  if isDryRun; then
    echo "sudo cp $src $dst && \\"
    echo "sudo chown $u:$u $dst && \\"
    echo "sudo chmod $p $dst"
    echo ""
  else
    # echo  USER: "$u" PERMISSION: $p  cp "$src" "$dst"
    sudo cp "$src" "$dst" && sudo chown "$u:$u" $dst && sudo chmod $p "$dst"
    sudo ls -lh "$dst"
  fi
}

################################################################################



function show_script_location() {
  echo ""
  echo '    set $fi_root '$SCRIPTS_DIR/$FI_APP_NAME/src;
  echo ""
  echo "    location = /fancyindex-logic.php {"
  echo "        include snippets/fastcgi-php.conf;"
  echo "        fastcgi_pass unix:/run/php/php-fpm.sock;"
  echo "        root ${fi_root};"
  echo "    }"
  echo "    location = /fancyindex-render.php {"
  echo "        include snippets/fastcgi-php.conf;"
  echo "        fastcgi_pass unix:/run/php/php-fpm.sock;"
  echo "        root ${fi_root};"
  echo "    }"
  echo ""
  echo "# Note: include snippests/fastcgi-php.conf - use file from nginx config:"
  echo "/etc/nginx/snippets/fastcgi-php.conf"
  echo ""
}

function show_root_location() {
  echo "    location / {"
  echo "        fancyindex on;"
  echo "        fancyindex_exact_size off;"
  echo "        fancyindex_header $WWW_DIR_RELATIVE/header.html;"
  echo "        fancyindex_footer $WWW_DIR_RELATIVE/footer.html;"
  echo "        fancyindex_css_href $WWW_DIR_RELATIVE/style.css;"
  echo '        fancyindex_time_format "%B %e, %Y";'
  echo '        try_files $uri.html $uri $uri/ =404;'
  echo "        root $FILESERV_ROOT;"
  echo "    }"
}





# Installation

function cp_statics_css_js() {
  local DIR="$1"
  echo "Copy Statics CSS JS Files into $DIR"
  mkdir_ex $PHP_FPM_USER "$DIR/js/"
  mkdir_ex $PHP_FPM_USER "$DIR/css/"
  cp_ex $PHP_FPM_USER $PERM "./html/js/archived_dir.js" "$DIR/js/"
  cp_ex $PHP_FPM_USER $PERM "./html/css/buttons.css" "$DIR/css/"
  cp_ex $PHP_FPM_USER $PERM "./html/css/msgbox.css" "$DIR/css/"
}

function cp_static_html() {
  echo "Copy StaticFiles into $WWW_DIR"
  mkdir_ex $NGINX_USER "$WWW_DIR"
  # frontend: form with buttons
  cp_ex $NGINX_USER $PERM_WWW  "./html/footer.html" "$WWW_DIR/"

  # optional not copy if files already exists
  if [ ! -f "$WWW_DIR/header.html" ]; then
    cp_ex $NGINX_USER $PERM_WWW "./html/header.html" "$WWW_DIR/"
  else
    echo "Skipped cmd: 'cp ./html/header.html $WWW_DIR' [File Alredy Exists]"
  fi
  if [ ! -f "$WWW_DIR/style.css" ]; then
    cp_ex $NGINX_USER $PERM_WWW "./html/style.css" "$WWW_DIR/"
  else
    echo "Skipped cmd: 'cp ./html/style.css $WWW_DIR' [File Alredy Exists]"
  fi
  cp_statics_css_js "$WWW_DIR"
}

# Only two php files must be accessebly from outside:
# - fancyindex-logic.php   -- busness logic for compression
# - fancyindex-render.php  -- genereate dynamic pages
# other files should be located in the directory where the main scripts
function install_src() {
  DST="$SCRIPTS_DIR/$FI_APP_NAME/src/"
  echo "Copy SRC into '$DST' "
  if isDryRun; then
     echo 'Destination for scripts is $SCRIPTS_DIR/$FI_APP_NAME/src/'
  fi
  mkdir_ex $PHP_FPM_USER "$DST"
  # backend: busness logic
  cp_ex $PHP_FPM_USER $PERM "./src/fancyindex-logic.php" "$DST"
  cp_ex $PHP_FPM_USER $PERM "./src/fancyindex-render.php" "$DST"
  cp_ex $PHP_FPM_USER $PERM "./src/pages.php" "$DST"
  cp_ex $PHP_FPM_USER $PERM "./src/checkhealth.php" "$DST"
  cp_ex $PHP_FPM_USER $PERM "./src/util.php" "$DST"
  cp_ex $PHP_FPM_USER $PERM "./src/compressed-db.php" "$DST"
  cp_ex $PHP_FPM_USER $PERM "./src/_page.html" "$DST"
  if [ "$1" != "--skip-config" -a "$1" != "-c" ]; then
    cp_ex $PHP_FPM_USER $PERM_CONF "./src/config.php" "$DST"
  elif [ ! -f "$DST/config.php" ]; then
    echo "[WARN] Not Found Config at $DST/config.php"
  else
    echo "Skipped: no-cp Config into $DST [DevOnly]"
  fi
}


function integrate_with_nginxy_theme() {
  [ -z $NGINXY_PATH -o $NGINXY_PATH == "/" ] && exit 1
  ANHOR="This footer will change depending on your settings"
  DIR="$FILESERV_ROOT/$NGINXY_PATH"
  echo "Install Script into Nginxy at '$DIR'";
  if [ ! -d "$DIR" ]; then
    echo "Not Found Directory with Nginxy '$DIR'"
    exit 1;
  else
    echo "TODO check Nginxy-Theme"
  fi

  FN="footer.html"
  INS="./html/$FN"
  DST="$DIR/$FN"
  # FUNC=$(declare -f ins2file_after_anhor)
  # sudo bash -c "$FUNC; ins2file_after_anhor $INS $DST '$ANHOR'" > "$FN"
  # cp_ex $PHP_FPM_USER $PERM "$FN" "$DIR/"

  cp_ex $PHP_FPM_USER $PERM "$INS" "$DIR/"
  cp_statics_css_js "$DIR"
}

function showVariables() {
  echo "[FI_APP_NAME]  :  $FI_APP_NAME"
  echo "[HIDDEN]       :  $HIDDEN"
  echo "[SCRIPTS_DIR]  :  $SCRIPTS_DIR"
  echo ""
  echo "Path to install script: "
  echo "Scripts(php)   :  $SCRIPTS_DIR/$FI_APP_NAME/src"
  echo '                  $SCRIPTS_DIR/$FI_APP_NAME/src'
  echo ""
  echo "Path to install static-html files:"
  echo "Static(html)   :  $WWW_DIR"
  echo '                  $FILESERV_ROOT/$HIDDEN/$FI_APP_NAME'
  echo ""
  echo "Path to root-dir of your File Server"
  echo "[FILESERV_ROOT]:  $FILESERV_ROOT"
  echo ""
  echo "Nginxy-Theme for FancyIndex (Optional) used only for ./$CMD nginxy"
  echo "[NGINXY_PATH]  :  $NGINXY_PATH"
  echo "  (full path)  :  $FILESERV_ROOT/$NGINXY_PATH"
  echo '               :  $FILESERV_ROOT/$NGINXY_PATH'
  echo ""
}


# Perfome main commands
OK=""

# Variables used in installation
if [ "$1" == "-v" -o "$1" == "--show-vars" ]; then
  showVariables
  shift
  OK="1"
fi


# Show parts for Nginx config
if [ "$1" == "-l" -o "$1" == "--show-locations" ]; then
  show_root_location
  show_script_location
  shift
  OK="1"
fi


if [[ "$1" == "-"* ]]; then
  echo "Uknown option $1"
  echo "See --help"
  exit 1
fi


# Install integration with already installed Fancy_Index + Ngingy-Theme
# NGINXY_PATH="/.nginx"
# n [--skip-config] [--dry-run]
if [ "$1" == "nginxy" -o "$1" == "n" ]; then
  shift
  checkAndDefineDryRun $@
  integrate_with_nginxy_theme
  install_src $@
  exit 0

# Install integration only for already installed Fancy_Index
# sa [--skip-config] [--dry-run]
elif [ "$1" == "standalone" -o "$1" == "s" ]; then
  shift
  checkAndDefineDryRun $@
  cp_static_html
  install_src $@
  exit 0

elif [ -z "$OK" ]; then
  echo "Uknown command $1"
fi






